<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends My_Front {

	public $token = "";

	function __construct(){
		parent::__construct();
	}
	public function index()
	{
		if ($this->input->post('sign')) {	
			$this->proceed();
		}
		else{
			$this->load->view('login');		
		}
	}
	

	function proceed(){
		if ($this->input->post('sign')) {						
			
			$dataUser=$this->User->get_by('email',$this->input->post('username'),0,1);
			if ($dataUser <> 0) {
				$roleID = SUPERADMIN;
				if($this->User->check_user($roleID)){
					$this->session->set_userdata(SUPERADMIN_SESSION, $id);
					redirect('admin/dashboard');
				}
				else{
					$this->session->set_flashdata('gagal', "Login failed or your email admin is not verified.");
					redirect(base_url());
				}
			}else{
				$this->session->set_flashdata('gagal', "Login failed.");
				redirect(base_url());
			}

		}
		else{			
			redirect(base_url());
		}
	}

	function redirects($uri,$id){
		
		// $this->session->set_userdata("user", $id);

		if ($uri=='logistik') {
			redirect('admin/transaksi/tambah');
		}
		else if ($uri=='supervisor') {
			redirect('admin/transaksi/inventory_kurir_staff');
		}
		else if ($uri=='admin_kota') {
			redirect('admin/dashboard');
		}else{
			return "NIL";
		}
		
	}

	function thankyou(){
		$this->load->view("say_thanks");
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends My_Front {

	function __construct(){
        parent::__construct();
    }

    function index(){
    	$data['all']=$this->Category->get();	
		$data['body']="category/all";	
		$this->load->view(admin(),$data);
    }	

    function delete($id){
		$this->Category->delete($id); 
		redirect('admin/category');
	}
	
	function edit($param=""){
		$data['edit']= ($param=='') ? false : true ;
        $data['body']= 'category/edit';
        if (save()) {
            if ($param=="") {
                $this->Category->insert_normal();
                redirect('admin/category');
            }else{
                $this->Category->update_data($param);
                redirect('admin/category');
            }
        }
        if ($param!="") {
            $data['data']=$this->Category->get_by_id($param);
        }
        $this->load->view(admin(),$data);
	}

}

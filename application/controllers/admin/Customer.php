<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends My_Front {

	function __construct(){
        parent::__construct();
    }

    function index(){
		$data['all']=$this->User->get_by_multiple('role_id',CUSTOMER,'is_deleted',0);	
		$data['body']="customer/all";	
		$this->load->view(admin(),$data);
    }	

    function edit($param=""){
		$data['edit']= ($param=='') ? false : true ;
        $data['body']= 'customer/edit';
        if (save()) {
            if ($param=="") {
                $this->User->insert_normal();
                redirect('admin/customer');
            }else{
                $this->User->update_data($param);
                redirect('admin/customer');
            }
        }
        if ($param!="") {
            $data['data']=$this->User->get_by_id($param);
            $data['dataAddress']=$this->UserAddress->get_by('user_id',$param,0,1);
        }
        $this->load->view(admin(),$data);
	}

	function delete($id){
		$dataOmos = array(
			'is_deleted' => 1, 
		);
		$this->User->update_field('id',$id,$dataOmos); 
		redirect('admin/customer');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends My_Front {

	function __construct(){
        parent::__construct();
        // if (!superAdminSession()) {
        // 	$this->session->sess_destroy();
        //     redirect(base_url());
        // }
    }

    function index(){
    	$data['role'] = false;
		$data['all']=$this->Order->getAllOrders();	
		$data['body']="order/all";	
		$this->load->view(admin(),$data);
    }	

}

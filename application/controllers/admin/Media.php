<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends My_Front {

	function __construct(){
        parent::__construct();
    }

    function index(){
		$data['all']=$this->Media->getNotDelete();	
		$data['body']="media/all";	
		$this->load->view(admin(),$data);
    }	

    function uploadData($action,$id=""){
    	$this->configUpload();
        if($this->upload->do_upload('file'))
        {        
            $finfo=$this->upload->data();     
            $this->createThumbnail($finfo['file_name']);   
            $dataMedia = array( 
                'filename' => "/".$finfo['file_name'],                  
                'filename_thumb' => "/".$finfo['raw_name']. '_thumb' .$finfo['file_ext'],
                'note' => $this->input->post('note'),
            );
            if ($action=='insert') {
                $res = $this->Media->insert_to($dataMedia);
                if ($res==true) {
                    redirect('admin/media');
                }
            }else{
                $this->Media->update($id,$dataMedia);
            }
        }
        else{
        	$errors = $this->upload->display_errors();
        	failedMsg($errors);
            if ($action=='insert') {
                redirect('admin/media/edit');
            }else{
                $url = "admin/media/edit/".$id;
                redirect($url);
            }
        }  
    }

    function edit($param=""){
		$data['edit']= ($param=='') ? false : true ;
        $data['body']= 'media/edit';
        if (save()) {
            if ($param=="") {
            	$this->uploadData('insert');
            	successMsg();
            }else{
                $this->uploadData('update',$param);
                successMsg();
            }
        }
        if ($param!="") {
            $data['data']=$this->Media->get_by_id($param);
        }
        $this->load->view(admin(),$data);
	}

	function delete($id){
		$data = array(
			'is_deleted' => 1, 
		);
		$this->Media->update_field('id',$id,$data); 
		redirect('admin/media');
	}
}

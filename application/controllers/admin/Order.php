<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends My_Front {

	function __construct(){
        parent::__construct();
    }

    function index(){
		$data['all']=$this->Order->getAllOrders();	
		$data['body']="order/all";	
		$this->load->view(admin(),$data);
    }	

    function view($orderId){
    	if ($this->input->post('btn-validate')) {
    		$dataOrder = array(
    			'status' => PAYMENT_SUCCESS, 
    		);
			$this->Order->update_field('id',$orderId,$dataOrder);    		
    	}
    	if ($this->input->post('btn-failed-validate')) {
    		$dataOrder = array(
    			'status' => PAYMENT_FAILED, 
    		);
			$this->Order->update_field('id',$orderId,$dataOrder);
    	}

    	$data['orderData']=$this->Order->getAllOrdersById($orderId);	
    	$data['orderDetail']=$this->OrderDetail->listOrderDetail($orderId);
    	$data['orderAddress'] = $this->OrderAddress->get_by("order_id",$orderId,0,1);
		$data['body']="order/view";	
		$this->load->view(admin(),$data);
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends My_Front {

	function __construct(){
        parent::__construct();
    }

    function index(){
		$data['all']=$this->Page->get();	
		$data['body']="pages/all";	
		$this->load->view(admin(),$data);
    }	

    function edit($param=""){
		$data['edit']= ($param=='') ? false : true ;
        $data['body']= 'pages/edit';
        if (save()) {
            $this->Page->update_data($param);
            redirect('admin/page');
        }

        if ($param=="") {
            redirect('admin/page');
        }
        
        $data['data']=$this->Page->get_by_id($param);
        
        $this->load->view(admin(),$data);
	}

	function delete($id){
		$dataOmos = array(
			'is_deleted' => 1, 
		);
		$this->User->update_field('id',$id,$dataOmos); 
		redirect('admin/page');
	}

}

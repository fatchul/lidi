<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends My_Front {

	function __construct(){
        parent::__construct();
    }

    function index(){
    	$data['all']=$this->Product->get();	
		$data['body']="product/all";	
		$this->load->view(admin(),$data);
    }	

    function delete($id){
		$this->Product->delete($id); 
		redirect('admin/product');
	}
	
	function edit($param=""){
		$data['edit']= ($param=='') ? false : true ;
        $data['all']=$this->Media->getNotDelete();  
        $data['body']= 'product/edit';
        if (save()) {
            if ($param=="") {
                $productId = $this->Product->insert_normal();
                $mediaData = array();
                foreach($_POST['media_id'] as $key=>$val){
                    array_push($mediaData, $key);     
                }

                if (count($mediaData) > 0) {
                    foreach ($mediaData as $key => $value) {
                        $isMain = 0;
                        if ($key == 0) {
                            $isMain = 1;
                        }
                        $dataProductMedia = array(
                            'product_id' => $productId,
                            'media_id' => $value,
                            'is_main' => $isMain 
                        );
                        $this->ProductMedia->insert_to($dataProductMedia);
                    }
                }
                redirect('admin/product');
            }else{

                if ($this->input->post('media_id')) {
                    $mediaData = array();
                    foreach($_POST['media_id'] as $key=>$val){
                        array_push($mediaData, $key);     
                    }
                    
                    if (count($mediaData) > 0) {
                        $this->ProductMedia->delete_by('product_id',$param);
                        foreach ($mediaData as $value) {
                            $dataProductMedia = array(
                                'product_id' => $param,
                                'media_id' => $value 
                            );
                            $this->ProductMedia->insert_to($dataProductMedia);
                        }
                    }
                }

                $this->Product->update_data($param);
                redirect('admin/product');
            }
        }
        if ($param!="") {
            $data['data']=$this->Product->get_by_id($param);
        }
        $this->load->view(admin(),$data);
    }

}

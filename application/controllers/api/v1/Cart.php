<?php
   
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
     
class Cart extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public $userId = "";
    
    public function __construct() {
       parent::__construct();
       $this->load->model('Cart_model','Cart');
       $this->load->model('Product_model','Product');

       $auth = Authentication();
       if (!$auth) {
            return $this->checkAction(false,'authentication failed',401);
       }

       $this->userId = $auth['userId'];
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get()
	{
        $data = $this->Cart->getAllCart($this->userId);
        return $this->checkAction(true,$data,200);
    }
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $json = file_get_contents('php://input');
        $jsonData = json_decode($json);

        $productData = $this->Product->get_by_id($jsonData->product_id);
        $cartExist = $this->Cart->get_by_multiple('user_id',$this->userId,'product_id',$jsonData->product_id);       

        if (!$productData) {
            return $this->checkAction(false,"produk tidak terdaftar",400);
        }

        $currentStock = $productData[0]->stock;
        if ($jsonData->qty > $currentStock) {
            return $this->checkAction(false,"maaf stok barang tidak tersedia",400);
        }  

        if ($cartExist <> 0) {
            $dataCart = array(
                'qty' => $cartExist[0]->qty + $jsonData->qty,
            );

            $this->Cart->update_field_multiple('user_id',$this->userId,'product_id',$jsonData->product_id,$dataCart);
        }else{

            $price = $productData[0]->retail_price;
            $roleId = userData($this->userId)[0]->role_id; 
            if($roleId == OMOS){
                $price = $productData[0]->grocery_price;
            }

            $dataCart = array(
                'product_id' => $jsonData->product_id,
                'qty' => $jsonData->qty,
                'price' => $price,
                'user_id' => $this->userId
            );

            $this->Cart->insert_to($dataCart);
        }

        $data = $this->Cart->getAllCart($this->userId);
        return $this->checkAction(true,$data,200);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($cartId)
    {
        $json = file_get_contents('php://input');
        $jsonData = json_decode($json);

        $dataCart = array(
            'qty' => $jsonData->qty,
        );

        $this->Cart->update_field('id',$cartId,$dataCart);
        $data = $this->Cart->getAllCart($this->userId);
        return $this->checkAction(true,$data,200);    
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($cartId)
    {
        $this->Cart->delete($cartId);
        $data = $this->Cart->getAllCart($this->userId);
        return $this->checkAction(true,$data,200);
    }
    	
}
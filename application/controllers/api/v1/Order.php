<?php

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Order extends REST_Controller {

	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public $userId = "";
    
    public function __construct() {
     parent::__construct();
     $this->load->model('Cart_model','Cart');
     $this->load->model('Order_model','Order');
     $this->load->model('Order_detail_model','OrderDetail');
     $this->load->model('User_address_model','UserAddress');
     $this->load->model('Order_address_model','OrderAddress');
     $this->load->model('Product_model','Product');
     $auth = Authentication();
     if (!$auth) {
      return $this->checkAction(false,'authentication failed',401);
    }

    $this->userId = $auth['userId'];
  }

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_get($id = 0)
    {
      if (!empty($id)) {
        $data = $this->Order->getAllOrdersByUserAndId($this->userId,$id);
      }else{
        $data = $this->Order->getAllOrdersByUser($this->userId);
      }

      $orderDatas = array();
      if ($data<>0) {
        foreach ($data as $key => $order) {
          $orderData = new stdClass();
          $orderData->id = $order->id;
          $orderData->order_code = $order->order_code;
          $orderData->status = $order->status;
          $orderData->note = $order->note;
          $orderData->path_proof_payment = getenv('PAYMENT_PROOF_IMAGE_BASE_URL')."".$order->path_proof_payment;
          $orderData->total_amount_order = $order->total_amount_order;
          $orderData->created_at = $order->created_at;
          $currentTime = currentTime(); 
          if($currentTime > $order->expired_payment){
            //order expired
            $orderData->text_status = statusPayment(ORDER_EXPIRED)['note'];
            $orderData->hex = statusPayment(ORDER_EXPIRED)['hex'];
          }else{
            $orderData->text_status = statusPayment($order->status)['note'];
            $orderData->hex = statusPayment($order->status)['hex'];
          }

          $orderData->expired_payment = $order->expired_payment;
          $orderDetailData = $this->OrderDetail->listOrderDetail($order->id);
          $orderData->order_detail = $orderDetailData;
          $orderAddressData = $this->OrderAddress->get_by("order_id",$order->id,0,1);
          if (!$orderAddressData) {
            $orderData->delivery_address = array();
          }else{
            $orderData->delivery_address = $orderAddressData;
          }
          
          array_push($orderDatas, $orderData);
        }
      }

      return $this->checkAction(true,$orderDatas,200);
    }	

    public function index_post()
    {
      $json = file_get_contents('php://input');
      $jsonData = json_decode($json);
      if (sizeof($jsonData->cart_ids) < 1) {
        return $this->checkAction(false,"cart empty",400);
      }

      $dataOrder = array(
        'user_id' => $this->userId, 
        'order_code' => strtoupper(generateRandomString(10)),
        'status' => WAITING_FOR_PAYMENT,
        'expired_payment' => modifyTime('PT12H'),
        "note" => (isset($jsonData->note)) ? $jsonData->note : "-",
      );

      $orderId = $this->Order->insert_to($dataOrder);
      $newOrderId = $orderId;

      $totalAmountOrder = 0;
      for ($i=0; $i < sizeof($jsonData->cart_ids); $i++) { 
        $dataCart = $this->Cart->get_by_id($jsonData->cart_ids[$i]);
        if (!$dataCart) {
          return $this->checkAction(false,"cart tidak terdaftar",400);
        }

        $dataOrderDetail = array(
          'order_id' => $orderId, 
          'product_id' => $dataCart[0]->product_id,
          'price' => $dataCart[0]->price,
          'qty' => $dataCart[0]->qty
        );
        $subTotal = (int)$dataCart[0]->qty * (int)$dataCart[0]->price;          
        $totalAmountOrder+=$subTotal;

        $productData = $this->Product->get_by_id($dataCart[0]->product_id);
        if (!$productData) {
            return $this->checkAction(false,"produk tidak terdaftar",400);
        }

        $currentStock = $productData[0]->stock;
        if ($dataCart[0]->qty > $currentStock) {
            return $this->checkAction(false,"maaf stok barang tidak tersedia",400);
        }  

        $this->OrderDetail->insert_to($dataOrderDetail);
      }

      $dataUpdateOrder = array(
        'total_amount_order' => $totalAmountOrder, 
      );
      $orderId = $this->Order->update_field('id',$orderId,$dataUpdateOrder);


      $dataAddress = $this->UserAddress->get_by("user_id",$this->userId,0,1);
      $dataOrderAddress = array(
        'order_id' => $newOrderId,
        'address' => $dataAddress[0]->address, 
      );
      $orderId = $this->OrderAddress->insert_to($dataOrderAddress);

      $data = $this->Order->getAllOrdersByUserAndId($this->userId,$newOrderId);

      $this->index_get($newOrderId);
    } 

    function paymentProof_post($orderCode=""){
      if (empty($orderCode)) {
        return $this->checkAction(false,"request tidak lengkap",400);  
      }
      $orderData = $this->Order->get_by('order_code',$orderCode);
      if ($orderData < 0) {
        return $this->checkAction(false,"kode order tidak ditemukan",400);
      }
      if ($orderData[0]->user_id!=$this->userId) {
        return $this->checkAction(false,'authentication failed',401);
      }
      if ($orderData[0]->status == PAYMENT_SUCCESS) {
        return $this->checkAction(false,'anda sudah berhasil melakukan pembayaran',400);
      }

      $orderDetails = $this->OrderDetail->listOrderDetail($orderData[0]->id);
      foreach ($orderDetails as $orderDetail) {
        $productData = $this->Product->get_by_id($orderDetail->product_id);
        $productStock = $productData[0]->stock;
        if ($orderDetail->qty > $productStock) {
          return $this->checkAction(false,'stok produk telah berubah atau kemungkinan sudah tidak tersedia',400);    
        }else{
          $this->Product->reduceStock($orderDetail->product_id,$orderDetail->qty);  
        } 
      }      
      
      if(!isset($_FILES["image_proof"])){
        return $this->checkAction(false,"file not found",400);  
      }

      foreach($_FILES as $index => $file) {
       move_uploaded_file($file['tmp_name'],'./tmpfile/'.$file['name']); 
      }

      $dataOrder = array(
        'status' => ORDER_PAID,
        'path_proof_payment' => "/".$file['name'], 
      );

      $orderId = $this->Order->update_field('order_code',$orderCode,$dataOrder);
      
      return $this->index_get($orderData[0]->id);
    }
  }

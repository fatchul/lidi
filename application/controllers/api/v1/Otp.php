<?php
   
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
     
class Otp extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->model('Otp_log_model','OTP');
       $this->load->model('User_model','User');
       $this->load->model('Session_log_model','SessionLog');
    }
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function request_post()
    {
        $json = file_get_contents('php://input');
        $jsonData = json_decode($json);

        if (isset($jsonData->full_name) && isset($jsonData->email)) {
            $dataUser = array(
                'email' => $jsonData->email, 
                'phone' => $jsonData->phone_number, 
                'full_name' => $jsonData->full_name,
                'role_id' => CUSTOMER
            );

            $userByEmail = $this->User->get_by('email',$jsonData->email);
            if ($userByEmail <> 0) {
                return $this->response( [
                    'message' => 'email sudah terdaftar',
                    'data' => null,
                ], REST_Controller::HTTP_BAD_REQUEST);
            }

            $userByPhone = $this->User->get_by('phone',$jsonData->phone_number);
            if ($userByPhone <> 0) {
                return $this->response( [
                    'message' => 'nomor hp sudah terdaftar',
                    'data' => null,
                ], REST_Controller::HTTP_BAD_REQUEST);
            }

            $userInserted = $this->User->insert_to($dataUser);
            if (!$userInserted) {
                return $this->checkAction(false,null,400);
            }
        }
            
        $randomNumber = rand(100000,999999);

        $dataOTP = array(
            'userKey' => getenv('OTP_USER_KEY'),
            'passKey' => getenv('OTP_API_KEY'),
            'phone' => $jsonData->phone_number,
            'message' => generateMessage($randomNumber), 
        );
        sendOTP($dataOTP);

        $data = array(
            'phone_number' => $jsonData->phone_number, 
            'valid_until' => modifyTime('PT12M'),
            'otp_code' => $randomNumber
        );
        $otpId = $this->OTP->insert_to($data);

        return $this->response( [
            'message' => 'success',
            'data' => $jsonData->phone_number,
        ], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function validate_post()
    {
        $json = file_get_contents('php://input');
        $jsonData = json_decode($json);

        $otpExist = $this->OTP->get_by_multiple('phone_number',$jsonData->phone_number,'is_verified',0);
        if ($otpExist <> 0) {
            if ($otpExist[0]->otp_code == $jsonData->otp_code) {
                
                $userByPhone = $this->User->get_by('phone',$jsonData->phone_number);
                if ($userByPhone < 0) {
                    return $this->checkAction(false,'user not found',400);
                }

                $randomString = generateRandomString(60);

                $sessionData = array(
                    'access_token' => $randomString,
                    'user_id' => $userByPhone[0]->id,
                );
                $this->SessionLog->insert_to($sessionData);
                
                $isOmosUser = false;
                if ($userByPhone[0]->role_id == OMOS) {
                    $isOmosUser = true;
                }


                $data = array(
                    'access_token' => $randomString, 
                    'is_omos' => $isOmosUser, 
                );
                
                $dataOTP = array(
                    'is_verified' => 1
                );
                $otpId = $this->OTP->update_field_multiple('otp_code',$jsonData->otp_code,'phone_number',$jsonData->phone_number,$dataOTP);

                return $this->checkAction(true,$data,200);
            }else{
                return $this->checkAction(false,'otp fail',400);
            }
        }else{
            return $this->checkAction(false,'otp fail',400);
        }

    }
     
    function login_post(){
        $json = file_get_contents('php://input');
        $jsonData = json_decode($json);

        if (!isset($jsonData->phone_number)) {
            return $this->checkAction(false,"nomor telepon tidak boleh kosong",400);
        }

        if (!isset($jsonData->password)){
            return $this->checkAction(false,"password tidak boleh kosong",400);
        }

        $dataUser=$this->User->get_by('phone',$jsonData->phone_number,0,1);
        if ($dataUser < 1) {
            return $this->checkAction(false,"nomor hp tidak terdaftar",401);
        }

        if (!validatePassword($jsonData->password,$dataUser[0]->password)) {
            return $this->checkAction(false,"username dan password tidak cocok",401);
        }

        $randomString = generateRandomString(60);

        $sessionData = array(
            'access_token' => $randomString,
            'user_id' => $dataUser[0]->id,
        );
        $this->SessionLog->insert_to($sessionData);

        $isOmosUser = false;
        if ($dataUser[0]->role_id == OMOS) {
            $isOmosUser = true;
        }

        $data = array(
            'access_token' => $randomString, 
            'is_omos' => $isOmosUser, 
        );

        return $this->checkAction(true,$data,200);
    }

    function register_post(){
        $json = file_get_contents('php://input');
        $jsonData = json_decode($json);

        if (!isset($jsonData->phone_number)) {
            return $this->checkAction(false,"nomor telepon tidak boleh kosong",400);
        }

        if (!isset($jsonData->password)){
            return $this->checkAction(false,"password tidak boleh kosong",400);
        }

        if (!isset($jsonData->full_name)){
            return $this->checkAction(false,"nama tidak boleh kosong",400);
        }

        $userByPhone = $this->User->get_by('phone',$jsonData->phone_number);
        if ($userByPhone <> 0) {
            return $this->checkAction(false,'nomor hp sudah terdaftar',400);
        }

        $dataUser = array(
            'phone' => $jsonData->phone_number, 
            'full_name' => $jsonData->full_name,
            'role_id' => CUSTOMER,
            'password' => generateHash($jsonData->password),
        );

        $userInserted = $this->User->insert_to($dataUser);
        if (!$userInserted) {
            return $this->checkAction(false,null,400);
        }

        $randomString = generateRandomString(60);

        $sessionData = array(
            'access_token' => $randomString,
            'user_id' => $userInserted,
        );
        $this->SessionLog->insert_to($sessionData);

        $data = array(
            'access_token' => $randomString, 
            'is_omos' => false, 
        );

        return $this->checkAction(true,$data,200);
    }

    	
}
<?php
   
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
     
class Product extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->model('Product_model','Product');
       $this->load->model('Product_media_model','ProductMedia');
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->Product->getAllProducts($id);
        }else{
            $data = $this->Product->getAllProducts();
        }
            
        if ($data <> 0) {
            $products = array();
            foreach ($data as $product) {
                $images = array();
                $productMedias = $this->ProductMedia->productMedia($product->id);
                if ($productMedias <> 0) {
                    foreach ($productMedias as $pm) {
                        $image = new stdClass();
                        $image->url_thumb = getenv('IMAGE_BASE_URL').$pm->filename_thumb;
                        $image->url_original = getenv('IMAGE_BASE_URL')."/".$pm->filename;
                        $image->title = $pm->note;
                        array_push($images, $image);
                    }
                }

                $product->images= $images;
                array_push($products, $product);
            }
            $data = $products;
        }

        return $this->checkAction(true,$data,200);
    }


    public function minQtyOmos_get($id = 0)
    {   
        return $this->checkAction(true,100,200);
    }

}
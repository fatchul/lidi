<?php

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class User extends REST_Controller {

	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public $userId = "";
    
  public function __construct() {
     parent::__construct();
     $this->load->model('User_model','User');
     $this->load->model('User_address_model','UserAddress');

     $auth = Authentication();
     if (!$auth) {
      return $this->checkAction(false,'authentication failed',401);
    }

    $this->userId = $auth['userId'];
  }

  public function address_put(){
    $json = file_get_contents('php://input');
    $jsonData = json_decode($json);

    if (!isset($jsonData->address)) {
      return $this->checkAction(false,'alamat tidak boleh kosong',400);
    }

    if (!isset($jsonData->city)) {
      return $this->checkAction(false,'kota tidak boleh kosong',400);
    }

    $dataUser = array(
      'address' => $jsonData->address,
      'city' => $jsonData->city, 
    );

    $isAddressExist = $this->UserAddress->get_by('user_id',$this->userId);
    if (!$isAddressExist) {
      $dataUser["user_id"] = $this->userId;
      $this->UserAddress->insert_to($dataUser);
    }else{
      $this->UserAddress->update_field('id',$isAddressExist[0]->id,$dataUser);
    }
    return $this->index_get();
  }

  public function index_get()
  {
    $data = $this->User->getUserData($this->userId);
    if (count($data) <= 0) {
      return $this->checkAction(false,"user not found",401);
    }

    $dataAddress = $this->UserAddress->get_by("user_id",$this->userId,0,1);

    $data[0]->link_image_avatar = getenv('IMAGE_BASE_URL')."".getenv('AVATAR_DIR')."".$data[0]->link_image_avatar;
    
    $data[0]->is_omos = false;
    if ($data[0]->role_id == OMOS) {
      $data[0]->is_omos = true;
    }

    if (!$dataAddress) {
      $dataAddress = array();      
    }

    $res = (object) [
        'user_data' => $data[0],
        'address' => $dataAddress,
    ];

    return $this->checkAction(true,$res,200);
  }

  public function update_post(){

    $dataUser = array();
    
    if(!empty($_POST["username"])){
      $dataUser["username"] = $_POST["username"];
      $totalDataExist = $this->User->countByExcept('username',$dataUser["username"],'id',$this->userId);
      if ($totalDataExist > 0) {
        return $this->checkAction(false,"username sudah dipakai user lain",400);
      }
    }
    
    if(!empty($_POST["email"])){
      $dataUser["email"] = $_POST["email"];
      $totalDataExist = $this->User->countByExcept('email',$dataUser["email"],'id',$this->userId);
      if ($totalDataExist > 0) {
        return $this->checkAction(false,"email sudah dipakai user lain",400);
      }
    }
    
    if(!empty($_POST["full_name"])){
      $dataUser["full_name"] = $_POST["full_name"];
    }
    
    if(isset($_FILES["image_avatar"])){
      foreach($_FILES as $index => $file) {
        move_uploaded_file($file['tmp_name'],'./tmpfile/profile/'.$file['name']); 
      }
      $dataUser['link_image_avatar'] = "/".$file['name']; 
    }

    $this->User->update_field('id',$this->userId,$dataUser);

    return $this->index_get();
  } 

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function omosRegistration_post()
    {
      $json = file_get_contents('php://input');
      $jsonData = json_decode($json);

      if (!isset($jsonData->username)) {
        return $this->checkAction(false,'username harus ada',400);
      }

      if (!isset($jsonData->full_name)) {
        return $this->checkAction(false,'nama harus ada',400);
      }

      if (!isset($jsonData->address)) {
        return $this->checkAction(false,'alamat tidak boleh kosong',400);
      }

      if (!isset($jsonData->city)) {
        return $this->checkAction(false,'kota tidak boleh kosong',400);
      }

      if (!isset($jsonData->email)) {
        return $this->checkAction(false,'email tidak boleh kosong',400);
      }

      $dataUser = array(
        'username' => $jsonData->username,
        'full_name' => $jsonData->full_name,
        'email' => $jsonData->email,
        'role_id' => OMOS
      );

      $this->User->update_field('id',$this->userId,$dataUser);

      $dataAddress = array(
        'user_id' => $this->userId,
        'address' => $jsonData->address,
        'city' => $jsonData->city,
        'phone' => userData($this->userId)[0]->phone,
      );

      $this->UserAddress->insert_to($dataAddress);

      return $this->index_get();
    }	

  }

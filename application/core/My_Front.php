<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class My_Front extends CI_Controller{
	function __construct(){
        parent::__construct();
        $this->load->model('User_model','User');
        $this->load->model('User_address_model','UserAddress');
        $this->load->model('Cart_model','Cart');
	    $this->load->model('Order_model','Order');
     	$this->load->model('Order_detail_model','OrderDetail');
     	$this->load->model('Category_model','Category');
     	$this->load->model('Product_model','Product');
     	$this->load->model('Media_model','Media');
        $this->load->model('Order_address_model','OrderAddress');
        $this->load->model('Product_media_model','ProductMedia');
        $this->load->model('Page_model','Page');
    }

    protected function configUpload(){
        $config['upload_path']   =   getenv('PRODUCT_DIR');
        $config['allowed_types'] =   "gif|jpg|jpeg|png"; 
        $config['max_size']      =   3000;
        $config['max_width']     =   3999;
        $config['max_height']    =   3999;
        $config['encrypt_name'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload',$config); 
        $this->upload->initialize($config); 
    }

    protected function createThumbnail($filename)
    {
        $config['image_library']    = "gd2";      
        $config['source_image']     = "tmpfile/".$filename;     
        $config['create_thumb']     = TRUE;       
        $config['maintain_ratio']   = FALSE;       
        $config['width'] = 100;       
        $config['height'] = 100;
        $this->load->library('image_lib',$config);
        $this->upload->initialize($config); 
        if(!$this->image_lib->resize()) 
        { 
            return $this->image_lib->display_errors(); 
        }  
        $this->image_lib->clear();
    }
}
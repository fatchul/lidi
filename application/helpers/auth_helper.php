<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('auth_helper'))
{
    function getRequestHeaders() {
        $headers = array();
        foreach($_SERVER as $key => $value) {
            if (substr($key, 0, 5) <> 'HTTP_') {
                continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $value;
        }
        return $headers;
    }


    function Authentication()
    {
        $headers = getRequestHeaders();

        foreach ($headers as $header => $value) {
            if (strtolower($header) != strtolower(HEADER)) {
                continue;
            }else{
                $CI = get_instance();                
                $CI->load->model('Session_log_model','SessionLog');
                $d = $CI->SessionLog->get_by('access_token',$value);
                if ($d <> 0) {
                    $data = array(
                        'userId' => $d[0]->user_id, 
                    );
                    return $data;
                }else{
                    return false;    
                }
            }
        }
    }

    function ValidatePassword($password,$hash){
        $result=hash_equals($hash, crypt($password, $hash));
        return $result;
    }

    function generateHash($password){
        // secure hashing of passwords using bcrypt, needs PHP 5.3+
        // see http://codahale.com/how-to-safely-store-a-password/
        // salt for bcrypt needs to be 22 base64 characters (but just [./0-9A-Za-z]), see http://php.net/crypt
        $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
        // 2y is the bcrypt algorithm selector, see http://php.net/crypt
        // 12 is the workload factor (around 300ms on my Core i7 machine), see http://php.net/crypt
        $hash = crypt($password, '$2y$12$' . $salt);//the $2y$12$ is an important notation, and shall not be removed. it determines the length of the hash generated and possibly the cause why the comparison failed. 
        // we can now use the generated hash as the argument to crypt(), since it too will contain $2y$12$... with a variation of the hash. No need to store the salt anymore, just the hash is enough!
        return $hash;
    }

    function superAdminSession(){
        $CI = get_instance();
        $CI->load->library('session');  
        echo $CI->session->userdata(SUPERADMIN_SESSION);die;       
        if ($CI->session->userdata(SUPERADMIN_SESSION)==TRUE) {
            return TRUE;
        }   
        return FALSE;
    }
}
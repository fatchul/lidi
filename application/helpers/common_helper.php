<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('common_helper'))
{
	function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
    
    function currentTime(){
        $date = new DateTime("now", new DateTimeZone('Asia/Jakarta') );
        return $date->format('Y-m-d H:i:s');
    }

    function modifyTime($modify){
        $date = new DateTime("now", new DateTimeZone('Asia/Jakarta') );
        $date->add(new DateInterval($modify));
        return $date->format('Y-m-d H:i:s');
    }
    
    function generateRandomString($length){
        $token = "";
        $codeAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
        }

        return $token;
    }

    function admin(){
        return "template/core";
    }

    function pricing($var)
    {
        $var = str_replace(',', '', $var);
        if (is_numeric($var)) {
            return number_format($var,0,",",".");
        }
        throw new \Exception("Invalid number to format: $var");
    }

    function save(){
         $CI = get_instance();
         $btsave=$CI->input->post('btn-save');
         if ($btsave==TRUE) {
             return TRUE;
         }
         return FALSE;
    }

    function statusPublish($par){
        if ($par==='0') {
            $data="<span class='btn btn-xs btn-warning'>Draft</span>";   
        }
        else{
            $data="<span class='btn btn-xs btn-success'>Publish</span>";   
        }
        return $data;        
    }

    function statusPayment($par){
        $data = array();
        if ($par==0) {
            $data['button']="<span class='btn btn-xs btn-warning'>Menunggu pembayaran</span>";   
            $data['note']="Menunggu pembayaran";
            $data['hex']="#f89f65";
        }
        else if ($par==1) {
            $data['button']="
                <button type='button' class='btn btx-xs btn-success' data-toggle='modal' data-target='#exampleModalCenter'>
                    Menunggu verifikasi pembayaran
                </button>
            ";  
            $data['note']="Verifikasi pembayaran"; 
            $data['hex']="#6ab5a6";
        }
        else if ($par==2) {
            $data['button']="<span class='btn btn-xs btn-warning'>Pembayaran telah di verifikasi</span>";   
            $data['note']="Pembayaran telah di verifikasi";
            $data['hex']="#a5f99b";
        }
        else if ($par==3) {
            $data['button']="<span class='btn btn-xs btn-warning'>Order di proses</span>";   
            $data['note']="Order di proses";
            $data['hex']="#2acaea";
        }
        else if ($par==4) {
            $data['button']="<span class='btn btn-xs btn-success'>Pembayaran gagal</span>";   
            $data['note']="Pembayaran gagal";
            $data['hex']="#cb2b46";
        }
        else if ($par==5) {
            $data['button']="<span class='btn btn-xs btn-danger'>Order kedaluarsa</span>";   
            $data['note']="Order Kedaluarsa";
            $data['hex']="#cb2b46";
        }
        else{
            $data['button']="<span class='btn btn-xs btn-danger'>Order gagal</span>";   
            $data['note']="Order gagal";
            $data['hex']="#cb2b46";
        }

        return $data;        
    }

    function successMsg(){
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->set_flashdata('sukses', "Sukses.");
    }
    function failedMsg($err){
        $CI = get_instance();
        $CI->load->library('session');
        return $CI->session->set_flashdata('gagal', $err);
    }
    function dates($param){
        if ($param) {
            list($tanggal,$jam)=explode(' ', $param);
            $newDate = date("d-m-Y", strtotime($tanggal));
            return $newDate." Jam ".$jam;
        }else{
            return "";
        }
    }
}
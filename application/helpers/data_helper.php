<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('common_helper'))
{

	function categoryList(){     
        $CI = get_instance();                
        $CI->load->model('Category_model','Category');
        $d = $CI->Category->get();
        $result=NULL;
        if ($d<>0) {
            $result['0'] = "--Pilih--";
            foreach ($d as $key => $value) {                
                $result[$value->id] = $value->name;
            }
        }
        return $result;
    }

    function userData($userId){     
        $CI = get_instance();                
        $CI->load->model('User_model','User');
        $d = $CI->User->get_by_id($userId);
        return $d;    
    }


}
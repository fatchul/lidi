<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('otp_helper'))
{

	function sendOTP($data)
    {
        $userkey = $data["userKey"];
        $passkey = $data["passKey"];
        $telepon = $data["phone"];
        $message = $data["message"];
        $url = 'https://console.zenziva.net/reguler/api/sendsms/';
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => $userkey,
            'passkey' => $passkey,
            'to' => $telepon,
            'message' => $message
        ));
        $results = json_decode(curl_exec($curlHandle), true);
        curl_close($curlHandle);
    }

    function generateMessage($otpCode){
        return "Jangan diberikan kepada orang lain, OTP CUTR anda adalah ".$otpCode;
    }

}
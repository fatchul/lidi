<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart_model extends Model_Main{    

    protected function get_table_name() {
        return 'cart';
    }
    protected function primary() {
        return 'id';
    }   
    function getAllCart($userId=0){
    	$q = "
    		SELECT 
            cart.id, 
            products.code, 
            products.name, 
            products.retail_price,
            products.grocery_price, 
            cart.qty,
            concat('".getenv('IMAGE_BASE_URL')."',media.filename_thumb) as link_image 
            FROM `cart` 
            left join products on products.id=cart.product_id 
            left join product_media on product_media.product_id=products.id 
            left join media on media.id=product_media.media_id
            where cart.user_id=? group by product_media.product_id
    	";

		return $this->db->query($q,array($userId))->result();
    }


}
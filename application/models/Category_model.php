<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_model extends Model_Main{    

    protected function get_table_name() {
        return 'category';
    }
    
    protected function primary() {
        return 'id';
    }   

    function get_all_field(){
	    $fields = array(                           
	     'name' => ucwords($this->input('name')),       
	    );
	    return $fields;
	}   
}
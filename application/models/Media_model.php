<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media_model extends Model_Main{    

    protected function get_table_name() {
        return 'media';
    }
    
    protected function primary() {
        return 'id';
    }   
}
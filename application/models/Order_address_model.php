<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_address_model extends Model_Main{    

    protected function get_table_name() {
        return 'order_address';
    }
    
    protected function primary() {
        return 'id';
    }   

}
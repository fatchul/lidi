<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_detail_model extends Model_Main{    

    protected function get_table_name() {
        return 'order_detail';
    }
    
    protected function primary() {
        return 'id';
    }   

    function listOrderDetail($orderId){
    	$q = "
    		select order_detail.product_id, products.name, order_detail.price, order_detail.qty
			from order_detail left join products on products.id=order_detail.product_id
			where order_detail.order_id=?
		";

		return $this->db->query($q,array($orderId))->result();
    }
}
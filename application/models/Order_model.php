<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_model extends Model_Main{    

    protected function get_table_name() {
        return 'order';
    }
    
    protected function primary() {
        return 'id';
    }   

    function getAllOrdersByUser($userId=0){
    	$q = "

    		SELECT 
			  `order`.*
			FROM `order` WHERE user_id = ? ORDER BY created_at DESC
		";

		return $this->db->query($q,array($userId))->result();
    }

    function getAllOrdersByUserAndId($userId=0,$orderId=0){
        $q = "
            SELECT 
              `order`.* 
            FROM `order` WHERE user_id = ? AND id= ? ORDER BY created_at DESC
        ";

        return $this->db->query($q,array($userId,$orderId))->result();
    }

    function getAllOrdersById($orderId=0){
        $q = "
            SELECT 
              `order`.*
            FROM `order` WHERE id= ? ORDER BY created_at DESC
        ";

        return $this->db->query($q,array($orderId))->result();
    }

    function getAllOrders(){
        $q = "

            SELECT 
              `order`.*
            FROM `order` ORDER BY created_at DESC
        ";

        return $this->db->query($q)->result();
    }
}
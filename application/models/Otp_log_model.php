<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Otp_log_model extends Model_Main{    

    protected function get_table_name() {
        return 'otp_log';
    }
    
    protected function primary() {
        return 'id';
    }   
}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page_model extends Model_Main{    

    protected function get_table_name() {
        return 'pages';
    }
    
    protected function primary() {
        return 'id';
    }   

    function get_all_field(){
	    $fields = array(                           
	     'slug' => ucwords($this->input('slug')),  
         'title' => $this->input('title'),
         'description' => $this->input('description'),       
	    );
	    return $fields;
	}   
}
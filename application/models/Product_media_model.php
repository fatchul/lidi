<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_media_model extends Model_Main{    

    protected function get_table_name() {
        return 'product_media';
    }
    
    protected function primary() {
        return 'id';
    }   

    function productMedia($productId){
    	$q = "
			SELECT media.filename,media.filename_thumb,media.note from product_media left join media on product_media.media_id=media.id where product_media.product_id=?
		";

		return $this->db->query($q,array($productId))->result();
    }
}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_model extends Model_Main{    

    protected function get_table_name() {
        return 'products';
    }
    protected function primary() {
        return 'id';
    }   
    function getAllProducts($productId=0){
    	$q = "
    		SELECT 
    			products.*, 6282221222045 as phone,
    			concat('".getenv('IMAGE_BASE_URL')."',media.filename_thumb) as link_image 
    		FROM `products` 
    		left join product_media on product_media.product_id=products.id 
    		left join media on media.id=product_media.media_id
    	";

    	if(!empty($productId)){
    		$q = $q." WHERE products.id = ? ";
    	}else{
            $q = $q." group by products.id";
        }

		return $this->db->query($q,array((int)$productId))->result();
    }

    function get_all_field(){
        $fields = array(                           
         'name' => $this->input('name'),
         'code' => $this->input('code'),
         'category_id' => $this->input('category_id'),
         'stock' => $this->input('stock'),
         'base_price' => $this->input('base_price'),       
         'retail_price' => $this->input('retail_price'),
         'grocery_price' => $this->input('grocery_price'),
         'status' => $this->input('status'),
         'description' => $this->input('description'),
        );

        return $fields;
    } 

    function reduceStock($productId,$qty){
        $q = "
          UPDATE products set stock = stock - ? where id = ?
        ";

        return $this->db->query($q,array((int)$qty,$productId));
    }

}
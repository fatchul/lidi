<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_address_model extends Model_Main{    

    protected function get_table_name() {
        return 'user_address';
    }
    
    protected function primary() {
        return 'id';
    }   
}
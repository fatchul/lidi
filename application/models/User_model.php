<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends Model_Main{    

    protected function get_table_name() {
        return 'users';
    }
    
    protected function primary() {
        return 'id';
    }   

    function getUserData($userId=0){
    	$q = "
    		SELECT 
			  id,username,email,full_name,phone,link_image_avatar,role_id
			FROM users WHERE id = ? AND is_deleted = 0
		";

		return $this->db->query($q,array($userId))->result();
    }

    function get_all_field(){
        $fields = array(                           
         'full_name' => $this->input('name'),
         'phone' => $this->input('phone'),
         'username' => $this->input('username'),
         'email' => $this->input('email'),
         'role_id' => $this->input('status'),      
        );
        
        return $fields;
    } 

    function check_user($roleID){
        $u=$this->input->post('username');
        $p=$this->input->post('password');
        $this->db->select('email, role_id');
        $this->db->from('users');
        $this->db->where('email', $u);        
        $this->db->where('role_id', $roleID);
        $this->db->where('is_deleted', 0);        
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $encPassword=$this->specific_column('password','email',$u);
            if (ValidatePassword($p,$encPassword)) {
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
}
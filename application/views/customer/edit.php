<?php echo open_bootstrap($edit ? "Edit Data Pelanggan" : "Tambah Pelanggan"); ?>
<?php echo form_open_multipart('',"class='form-horizontal'") ?>   
  <?php echo t_text('Nama','name', $edit ? $data[0]->full_name : '') ?>
  <?php echo t_text('No Hp','phone', $edit ? $data[0]->phone : '') ?> 
  <?php echo t_text('Username','username', $edit ? $data[0]->username : '') ?>
  <?php echo t_text('Email','email', $edit ? $data[0]->email : '','','','required') ?>
  <?php echo t_radio_select('Tipe User','status',OMOS,CUSTOMER,'One Man One Site','User Biasa',$edit ? $data[0]->role_id : '') ?>
  <?php echo t_textarea('Alamat','address', $edit ? ((!$dataAddress) ? '-': $dataAddress[0]->address) : '') ?>
  <?php echo t_text('Kota Tempat Tinggal','city', $edit ? ((!$dataAddress) ? '-': $dataAddress[0]->city) : '') ?>
  <?php echo t_submit('','btn-save',"Simpan") ?>                  
</form>
<?php echo close_bootstrap(); ?>
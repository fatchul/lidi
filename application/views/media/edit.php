<?php echo open_bootstrap($edit ? "Edit Media" : "Tambah Media"); ?>
<?php echo form_open_multipart('',"class='form-horizontal'") ?>   
  <?php if ($edit): ?>
  	<?php echo t_image('Gambar Sekarang',getenv('IMAGE_BASE_URL').$data[0]->filename) ?>
  <?php endif ?>
  <?php echo t_file('Update Gambar','file','') ?>
  <?php echo t_text('Catatan','note', $edit ? $data[0]->note : '') ?> 
  <?php echo t_submit('','btn-save',"Simpan") ?>                  
</form>
<?php echo close_bootstrap(); ?>
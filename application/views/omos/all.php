<?php echo open_bootstrap("One Man One Site"); ?>
<style type="text/css">
  .more{
    cursor: pointer;
  }
</style>
<div class="row">

<div class="col-md-12">

  <div class="table-responsive">
  <table id="example" class="table" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>No HP</th>
      <th>Email</th>
      <th></th>  
    </tr>
  </thead>
  <tbody>
    <?php if ($all <> 0): ?>
      <?php foreach ($all as $key => $value): ?>
        <tr>
          <td><?= $key+1 ?></td>
          <td>
            <?= $value->full_name ?> <br>
          </td>
          <td>
            <?= $value->phone ?> <br>
          </td>
          <td>
            <?= $value->email ?> <br>
          </td>
          <td>            
            <a class="btn btn-xs btn-warning" href='<?= base_url() ?>admin/omos/edit/<?php echo $value->id ?>' title="Edit" onclick=""><i class="glyphicon glyphicon-pencil"></i></a>
            <a class="btn btn-xs btn-danger" href='<?= base_url() ?>admin/omos/delete/<?php echo $value->id ?>' title="Hapus" ><i class="glyphicon glyphicon-trash"></i></a>
          </td>
        </tr>    
      <?php endforeach ?>
    <?php endif ?>
  </tbody>  
</table>
</div>
</div>
</div>
<?php echo close_bootstrap(); ?>

<script src="<?= base_url(); ?>asset/js/datatables/datatables.min.js"></script>


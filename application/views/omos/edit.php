<?php echo open_bootstrap($edit ? "Edit Data One Man One Site" : "Tambah Kategori"); ?>
<?php echo form_open_multipart('',"class='form-horizontal'") ?>   
  <?php echo t_text('Nama','name', $edit ? $data[0]->full_name : '') ?>
  <?php echo t_text('No Hp','phone', $edit ? $data[0]->phone : '') ?> 
  <?php echo t_text('Username','username', $edit ? $data[0]->username : '') ?>
  <?php echo t_text('Email','email', $edit ? $data[0]->email : '') ?>
  <?php echo t_textarea('Alamat','address', $edit ? ((!$dataAddress) ? '-': $dataAddress[0]->address) : '') ?>
  <?php echo t_text('Kota Tempat Tinggal','city', $edit ? ((!$dataAddress) ? '-': $dataAddress[0]->city) : '') ?>
  <?php echo t_text('Tanggal Bergabung','created_at', $edit ? $data[0]->created_at : '') ?>
  <?php echo t_submit('','btn-save',"Simpan") ?>                  
</form>
<?php echo close_bootstrap(); ?>
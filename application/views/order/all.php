<?php echo open_bootstrap("Order"); ?>
<style type="text/css">
  .more{
    cursor: pointer;
  }
</style>
<div class="row">

<div class="col-md-12">

  <div class="table-responsive">
  <table id="example" class="table" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Tanggal Order</th>
      <th>Nomor Order</th>
      <th>Nama Pelanggan</th>
      <th>Status Pembayaran</th>
      <th>No HP</th>
      <th>Total Transaksi</th>
      <th></th>  
    </tr>
  </thead>
  <tbody>
    <?php if ($all <> 0): ?>
      <?php foreach ($all as $key => $value): ?>
        <?php $user = userData($value->user_id) ?>
        <tr>
          <td><?= $key+1 ?></td>
          <td>
            <?= dates($value->created_at) ?> <br>
          </td>
          <td>
            <?= $value->order_code ?> <br>
          </td>
          <td>
            <?= ucwords($user[0]->full_name) ?> <br>
          </td>
          <td>
            <?= statusPayment($value->status)['button'] ?> <br>
          </td>
          <td>
            <?= $user[0]->phone ?>
          </td>
          <td>
            <?= pricing($value->total_amount_order) ?> <br>
          </td>
          <td>            
            <a class="btn btn-xs btn-warning" href='<?= base_url() ?>admin/order/view/<?php echo $value->id ?>' title="Edit" onclick=""><i class="glyphicon glyphicon-pencil"></i></a>
            <a class="btn btn-xs btn-danger" href='<?= base_url() ?>admin/order/delete/<?php echo $value->id ?>' title="Hapus" ><i class="glyphicon glyphicon-trash"></i></a>
          </td>
        </tr>    
      <?php endforeach ?>
    <?php endif ?>
  </tbody>  
</table>
</div>
</div>
</div>
<?php echo close_bootstrap(); ?>

<script src="<?= base_url(); ?>asset/js/datatables/datatables.min.js"></script>


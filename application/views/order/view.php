<?php $userData =  userData($orderData[0]->user_id) ; ?>
<?php //$orderUser = $this->OrderUser->get_by('id_order',$data[0]->id_order); ?>            
<div class="container">
    <div class="row">
        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <strong><?= ucfirst($userData[0]->full_name) ?></strong> 
                        <br>
                        <?php if ($orderAddress<>0): ?>
                            Alamat Pengiriman : 
                            <br>
                            <?= $orderAddress[0]->address ?>
                            <br>
                            <abbr title="Phone"></abbr> <?= $userData[0]->phone ?>
                        <?php endif ?>
                    </address>
                    Status Order : <?php echo statusPayment($orderData[0]->status)['button'] ?>
                    <br><br>
                    Tanggal Kadaluarsa Pembayaran : 
                    <br>
                    <?php echo $orderData[0]->expired_payment ?>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                        <em>Date: <?php echo $orderData[0]->created_at ?></em>
                    </p>
                    <p>
                        <em>Receipt #: <?php echo $orderData[0]->order_code ?></em>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                    <h1>Receipt</h1>
                </div>
                </span>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>#</th>
                            <th>Qty</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; $qty=0; ?>
                        <?php foreach ($orderDetail as $key => $value): ?>
                            <?php $qty += $value->qty; ?>
                            <?php $subtotal = $value->qty * $value->price; ?>
                            <?php $total += $subtotal ?>
                            <tr>
                                <td class="col-md-9"><?= $value->name ?></h4></td>
                                <td class="col-md-1" style="text-align: center"> <?= $value->product_id ?> </td>
                                <td class="col-md-1 text-center"><?= $value->qty ?></td>
                                <td class="col-md-1 text-center"><?= pricing($value->price) ?></td>
                                <td class="col-md-1 text-center"><?= pricing($value->price * $value->qty) ?></td>
                            </tr>    
                        <?php endforeach ?>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right">
                            <p>
                                <strong>Subtotal: </strong>
                            </p></td>
                            <td class="text-center">
                            <p>
                                <strong><?= pricing($total) ?></strong>
                            </p></td>
                        </tr>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right"><h4><strong>Total: </strong></h4></td>
                            <td class="text-center text-danger"><h4><strong><?= pricing($total) ?></strong></h4></td>
                        </tr>
                    </tbody>
                </table>
                </td>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <img src="<?= getenv('PAYMENT_PROOF_IMAGE_BASE_URL') ?><?= $orderData[0]->path_proof_payment ?>" width="270px">
          </div>
          <div class="modal-footer">
            <?php echo form_open("") ?>
                <input type="submit" class="btn btn-danger" name="btn-failed-validate" value="Pembayaran Gagal">
                <input type="submit" class="btn btn-primary" name="btn-validate" value="Proses Order">
            <form>
          </div>
        </div>
      </div>
    </div>
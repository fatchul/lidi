<?php echo open_bootstrap("Kategori"); ?>
<style type="text/css">
  .more{
    cursor: pointer;
  }
</style>
<div class="row">

<div class="col-md-12">

  <div class="table-responsive">
  <table id="example" class="table" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Halaman</th>
      <th>Deskripsi</th>
      <th></th>  
    </tr>
  </thead>
  <tbody>
    <?php if ($all <> 0): ?>
      <?php foreach ($all as $key => $value): ?>
        <tr>
          <td><?= $key+1 ?></td>
          <td>
            <?= $value->title ?> <br>
          </td>
          <td>
            <?= $value->description ?> <br>
          </td>
          <td>            
            <a class="btn btn-xs btn-warning" href='<?= base_url() ?>admin/page/edit/<?php echo $value->id ?>' title="Edit" onclick=""><i class="glyphicon glyphicon-pencil"></i></a>
          </td>
        </tr>    
      <?php endforeach ?>
    <?php endif ?>
  </tbody>  
</table>
</div>
</div>
</div>
<?php echo close_bootstrap(); ?>

<script src="<?= base_url(); ?>asset/js/datatables/datatables.min.js"></script>


<?php echo open_bootstrap($edit ? "Edit Halaman" : ""); ?>
<?php echo form_open_multipart('',"class='form-horizontal'") ?>   
  <?php echo t_text('Judul','title', $edit ? $data[0]->title : '') ?>
  <?php echo t_editor('Deskripsi','description', $edit ? $data[0]->description : '') ?> 
  <?php echo t_submit('','btn-save',"Simpan") ?>                  
</form>
<?php echo close_bootstrap(); ?>
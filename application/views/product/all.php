<?php echo open_bootstrap("Produk"); ?>
<style type="text/css">
  .more{
    cursor: pointer;
  }
</style>
<a  class="btn btn-info" role="button" href="<?php echo base_url('admin/product/edit') ?>">TAMBAH</a>
<br>
<br>
<div class="row">

<div class="col-md-12">

  <div class="table-responsive">
  <table id="example" class="table" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Produk</th>
      <th>Harga Awal</th>
      <th>Harga Ecer</th>
      <th>Harga Grosir</th>
      <th>Status</th>
      <th>Stok</th>
      <th></th>  
    </tr>
  </thead>
  <tbody>
    <?php if ($all <> 0): ?>
      <?php foreach ($all as $key => $value): ?>
        <tr>
          <td><?= $key+1 ?></td>
          <td>
            <?= $value->name ?> <br>
          </td>
          <td>
            <?= pricing($value->base_price) ?> <br>
          </td>
          <td>
            <?= pricing($value->retail_price) ?> <br>
          </td>
          <td>
            <?= pricing($value->grocery_price) ?> <br>
          </td>
          <td>
            <?= $value->status ?> <br>
          </td>
          <td>
            <?= $value->stock ?> <br>
          </td>
          <td>            
            <a class="btn btn-xs btn-warning" href='<?= base_url() ?>admin/product/edit/<?php echo $value->id ?>' title="Edit" onclick=""><i class="glyphicon glyphicon-pencil"></i></a>
            <a class="btn btn-xs btn-danger" href='<?= base_url() ?>admin/product/delete/<?php echo $value->id ?>' title="Hapus" ><i class="glyphicon glyphicon-trash"></i></a>
          </td>
        </tr>    
      <?php endforeach ?>
    <?php endif ?>
  </tbody>  
</table>
</div>
</div>
</div>
<?php echo close_bootstrap(); ?>

<script src="<?= base_url(); ?>asset/js/datatables/datatables.min.js"></script>


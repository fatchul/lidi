<?php echo open_bootstrap($edit ? "Edit Produk" : "Tambah Produk"); ?>
<?php echo form_open_multipart('',"class='form-horizontal'") ?>   
  <?php echo t_text('Nama Produk','name', $edit ? $data[0]->name : '') ?>
  <?php echo t_text('Kode Produk','code', $edit ? $data[0]->code : '') ?>
  <?php echo $edit ? dropselect('Kategori','category_id',categoryList(),$data[0]->category_id,true,'category') : drop_bootstrap('Kategori','category_id',categoryList(),'category') ?>
  <?php echo t_editor('Deskripsi','description', $edit ? $data[0]->description : '') ?>
  <?php echo t_number('Harga Beli','base_price', $edit ? $data[0]->base_price : '') ?>
  <?php echo t_number('Harga Ecer','retail_price', $edit ? $data[0]->retail_price : '') ?>
  <?php echo t_number('Harga Grosir','grocery_price', $edit ? $data[0]->grocery_price : '') ?> 
  <?php echo t_number('Stok','stock', $edit ? $data[0]->stock : '') ?> 
  <?php echo t_radio_select('Publish','status','1','0','Publish','Draft',1) ?>
  <hr>
  <h2>Silahkan pilih media gambar untuk produk ini</h2>
  <div class="col-md-12">
  	<div class="table-responsive">
  		<table id="example" class="table" cellspacing="0" width="100%">
  			<thead>
  				<tr>
  					<th>No</th>
  					<th>Media</th>
  					<th>Catatan</th>
  					<th></th>  
  				</tr>
  			</thead>
  			<tbody>
  				<?php if ($all <> 0): ?>
  					<?php foreach ($all as $key => $value): ?>
  						<tr>
  							<td><?= $key+1 ?></td>
  							<td>
  								<img src="<?= getenv('IMAGE_BASE_URL') ?><?= $value->filename_thumb ?>">
  							</td>
  							<td>
  								<?php echo $value->note ?>
  							</td>
  							<td>            
  								<input type="checkbox" name="media_id[<?= $value->id ?>]">
  							</td>
  						</tr>    
  					<?php endforeach ?>
  				<?php endif ?>
  			</tbody>  
  		</table>
  	</div>
  </div>
</div>

  <?php echo t_submit('','btn-save',"Simpan") ?>                  
</form>
<?php echo close_bootstrap(); ?>
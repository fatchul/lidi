<?php $this->load->view('template/header'); ?>
<div id="loading" class="loading" style="display: none;">Wait</div>
<?php $this->load->view('template/top'); ?>
<?php $this->load->view('template/menu'); ?>

<div class="content" ng-app="myApp" ng-controller="myCtrl as vm">
	<?php $this->load->view($body); ?>	 	
</div>

<?php $this->load->view('template/footer') ?>
<?php $this->load->view('template/notif') ?>
<?php $this->load->view('template/script'); ?>
</body>
</html>

<div class="row footer">
	<div class="col-md-6 text-left">
		© <?php echo date("Y") ?>
	</div>
	<div class="col-md-6 text-left">
		Design and Developed by <a href="https://instagram.com/fatchul_amin/" target="_blank">Fatchul Amin</a>
	</div> 
</div>

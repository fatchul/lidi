<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lidi</title>
  <meta name="robots" content="noindex, nofollow">
  <meta name="author" content="Lidi">
  <link rel="shortcut icon" href="Lidi">
  <link href="<?= base_url(); ?>asset/css/appsroot.css" rel="stylesheet">  
  <script type="text/javascript" src="<?= base_url(); ?>asset/js/jquery.min.js"></script>  
</head>
<body>
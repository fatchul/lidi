<li><a href="<?php echo site_url('admin/dashboard') ?>"><span class="icon color5"><i class="fa fa-home"></i></span>Dashboard</a>
  
<li><a href="#"><span class="icon color7"><i class="fa fa-money"></i></span>Master Data<span class='caret'></span></a>
  <ul>
    <li><a href="<?php echo site_url('admin/category') ?>">Kategori Produk</a></li>
    <li><a href="<?php echo site_url('admin/product') ?>">Produk</a></li>
    <li><a href="<?php echo site_url('admin/media') ?>">Media</a></li>
    <li><a href="<?php echo site_url('admin/page') ?>">Halaman</a></li>
  </ul>
</li>

<li><a href="#"><span class="icon color7"><i class="fa fa-archive"></i></span>Inventory<span class='caret'></span></a>
  <ul>
    <li><a href="<?php echo site_url('admin/omos') ?>">Data OMOS</a></li>
    <li><a href="<?php echo site_url('admin/customer') ?>">Data Customer</a></li>
  </ul>
</li>

<li><a href="#"><span class="icon color7"><i class="fa fa-book"></i></span>Laporan<span class='caret'></span></a>
  <ul>
    <li><a href="<?php echo site_url('admin/order') ?>">Data Order</a></li>
  </ul>
</li>

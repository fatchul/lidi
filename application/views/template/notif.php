	<style type="text/css">
		.pojok{
			position:fixed;
			bottom: 0px;
			left: 10px;
			float: left;
		}
	</style>
	<div class="pojok" id="alertbottomright">
		<?php if($this->session->flashdata('sukses')) { ?>
			<div class="kode-alert kode-alert-icon alert1-light">
				<i class="fa fa-check"></i>
				<a href="#" class="closed">&times;</a>
				<?php echo $this->session->flashdata('sukses'); ?>           
			</div>
		<?php } ?>
		<?php if($this->session->flashdata('gagal')) { ?>
		<div class="kode-alert kode-alert-icon alert9-light">
			<i class="fa fa-warning"></i>
			<a href="#" class="closed">&times;</a>
			<?php echo $this->session->flashdata('gagal'); ?>           
		</div>
		<?php } ?>
	</div>
	<script>
		var close = document.getElementsByClassName("closebtn");
		var i;

		for (i = 0; i < close.length; i++) {
			close[i].onclick = function(){
				var div = this.parentElement;
				div.style.opacity = "0";
				setTimeout(function(){ div.style.display = "none"; }, 6000);
			}
		}
	</script>

	<script type="text/javascript">$(document).ready(function() {$('#alertbottomright').click();$("#alertbottomright").fadeIn(50000);$('#alertbottomright').click();$("#alertbottomright").fadeOut(5000);});</script>
<style type="text/css">
  .hex:hover{
    color: blue;
  }
</style>
<div id="top" class="clearfix">
  	<div class="applogo">
  		<a href="#" class="logo">Clutr.My.ID</a>
  	</div>

      <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
      <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>  
    
    <ul class="top-right">
      <li class="dropdown link">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><img src="<?= base_url(); ?>asset/images/ico_sm.png" alt="img"><b><?php //echo getNameSessionData() ?></b><span class="caret"></span></a>
        <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
          <li class="divider"></li>
          <li><a href="<?php echo site_url('login/logout') ?>"><i class="fa falist fa-power-off"></i> Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>

  <script type="text/javascript">
    var elem = document.documentElement;
    function openFullscreen() {
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
      }
    }
  </script>
/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	config.removeButtons = '';
	config.uiColor = '#FFFFFF';
	config.language = 'en';
	
	config.height = 300;
	config.toolbarCanCollapse = true;


	// config.removePlugins = 'flash,smiley,table,tabletools,image,colordialog,zoom,forms,templates,widgets,wsc,widgetselection,uploadwidget,tableselection,specialchar,scayt,lineutils,magicline,dialog,clipboard,a11yhelp,div,find';
	// config.extraPlugins = 'zoom';

	
	// config.filebrowserBrowseUrl = 'http://www.arkademy.com/asset/plugins/kcfinder/browse.php?opener=ckeditor&type=files';
 //   config.filebrowserImageBrowseUrl = 'http://www.arkademy.com/asset/plugins/kcfinder/browse.php?opener=ckeditor&type=images';
 //   config.filebrowserFlashBrowseUrl = 'http://www.arkademy.com/asset/plugins/kcfinder/browse.php?opener=ckeditor&type=flash';
 //   config.filebrowserUploadUrl = 'http://www.arkademy.com/asset/plugins/kcfinder/upload.php?opener=ckeditor&type=files';
 //   config.filebrowserImageUploadUrl = 'http://www.arkademy.com/asset/plugins/kcfinder/upload.php?opener=ckeditor&type=images';
 //   config.filebrowserFlashUploadUrl = 'http://www.arkademy.com/asset/plugins/kcfinder/upload.php?opener=ckeditor&type=flash';
};
